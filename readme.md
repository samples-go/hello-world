# Abstract

This is an example about how to configure go development environment and create our first go program.


## Prerequisites 

This is an example made using a linux, Eclipe Spring Tool Suite and GoClipse plugin.


### Install & Configure Go

Follow the next steps:

1. Download and install Go compiler from the [official site](https://golang.org/dl/)  
2. Create environment variable `GOROOT` in the Go installation directory, for example `/usr/share/go`
3. Add `GOROOT\bin` to `PATH` variable
4. Create environment variable GOPATH in a different `GOROOT` directory, for example `/usr/share/go/globals`
5. Add `GOPATH\bin` to `PATH` variable


### Install & Configure Go Tools

1. Download GoClipse tool gocode from [GitHub](https://github.com/nsf/gocode) and install it in `usr/share/go/globals/bin`

```bash
$ go get -u github.com/nsf/gocode
```

2. Download GoClipse tool guru from [GitHub](https://godoc.org/golang.org/x/tools/cmd/guru) and install it in `usr/share/go/globals/bin`

```bash
$ go get   golang.org/x/tools/cmd/guru
```

3. Download GoClipse tool godef from [GitHub](https://github.com/rogpeppe/godef) and install it in `usr/share/go/globals/bin`

```bash
$ go get github.com/rogpeppe/godef
```

4. Download GoClipse tool gometaliner from [GitHub](https://github.com/alecthomas/gometalinter) and install it in `usr/share/go/bin`

```bash
$ curl -L https://git.io/vp6lP | sh
```	

### Install & Configure GoClipse

1. Open `Help\Marketplace` and search for GoClipse plugin and install it.
1. Access Eclipse preferences from the menu `Window / Preferences`, navigate to the `Go` preference page and set up the installation, for example:

```	bash
Directory = /usr/share/go
Use same value as GOPATH environment variable = unmarked
Eclipse GOPATH = /usr/share/go/globals
Also add project location to GOPATH if it is not contained there already = marked
```

3. Access Eclipse preferences from the menu `Window / Preferences`, navigate to the `Go\Tools` preference page and set up the installation, for example:

```bash
gocode = /usr/share/go/globals/bin/gocode
guru = /usr/share/go/globals/bin/guru
godef = /usr/share/go/globals/bin/godef
Use default location (from Go installation) = marked
 ```

## Project Features

This project contains the next features 

* hello_word.go: Main progam that just print a message.
using stringutil package.

 
 > **Note:** All features include tests.
 

# Resources

The information in this document and the next resources are provided by GoLang and GoClipse official public sites:

* Go language [official site](https://golang.org/)
* Go language [official documentation](https://golang.org/doc/)
* Go language [official play console](https://golang.org/doc/)
* GoClipse [official site](https://goclipse.github.io/)
* GoClipse [official documentation](https://github.com/GoClipse/goclipse/blob/latest/documentation/UserGuide.md#user-guide)
