package main

import (
	"fmt"
)

// Main function
func main() {

	/*
	   This is a multiline
	    comment
	*/

	// This is a one line comment
	fmt.Println("hello world")
}
